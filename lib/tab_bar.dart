/// Flutter code sample for TabBar

// This sample shows the implementation of [TabBar] and [TabBarView] using a [DefaultTabController].
// Each [Tab] corresponds to a child of the [TabBarView] in the order they are written.

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: TabBarComponent(),
    );
  }
}

class TabBarComponent extends StatelessWidget {
  const TabBarComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 1,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          bottom: const TabBar(
            unselectedLabelColor: Colors.black,
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
            labelColor: Colors.black,
            labelStyle: TextStyle(fontWeight: FontWeight.bold),
            indicatorColor: Color.fromRGBO(0, 165, 137, 1),
            indicatorWeight: 8,
            tabs: <Widget>[
              Tab(
                text: "Atividades",
              ),
              Tab(
                text: "Alunos",
              ),
              Tab(
                text: "Ocorrências",
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: <Widget>[
            Center(
              child: Text("página de atividades"),
            ),
            Center(
              child: Text("página de alunos"),
            ),
            Center(
              child: Text("página de ocorrências"),
            ),
          ],
        ),
      ),
    );
  }
}

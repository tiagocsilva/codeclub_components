import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  @override
  MyTextInputState createState() => new MyTextInputState();
}

class MyTextInputState extends State<Menu> {
  bool selected = false;
  bool selected2 = false;
  bool selected3 = false;
  bool selected4 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 500,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Text('Menu'),
            GestureDetector(
              onTap: () {
                setState(() {
                  selected = true;
                  selected2 = false;
                  selected3 = false;
                  selected4 = false;
                });
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  "Reporte Mensal",
                  style: TextStyle(
                      color: selected
                          ? Color.fromRGBO(0, 165, 137, 1)
                          : Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none),
                ),
                width: double.maxFinite,
                height: 50,
                decoration: BoxDecoration(
                  color:
                      selected ? Colors.white : Color.fromRGBO(0, 165, 137, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selected = false;
                  selected2 = true;
                  selected3 = false;
                  selected4 = false;
                });
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  "Material Didático",
                  style: TextStyle(
                      color: selected2
                          ? Color.fromRGBO(0, 165, 137, 1)
                          : Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none),
                ),
                width: double.maxFinite,
                height: 50,
                decoration: BoxDecoration(
                  color:
                      selected2 ? Colors.white : Color.fromRGBO(0, 165, 137, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selected = false;
                  selected2 = false;
                  selected3 = true;
                  selected4 = false;
                });
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  "Documentação",
                  style: TextStyle(
                      color: selected3
                          ? Color.fromRGBO(0, 165, 137, 1)
                          : Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none),
                ),
                width: double.maxFinite,
                height: 50,
                decoration: BoxDecoration(
                  color:
                      selected3 ? Colors.white : Color.fromRGBO(0, 165, 137, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selected = false;
                  selected2 = false;
                  selected3 = false;
                  selected4 = true;
                });
              },
              child: Container(
                alignment: Alignment.centerLeft,
                child: new Text(
                  "Feedback",
                  style: TextStyle(
                      color: selected4
                          ? Color.fromRGBO(0, 165, 137, 1)
                          : Colors.white,
                      fontSize: 18,
                      decoration: TextDecoration.none),
                ),
                width: double.maxFinite,
                height: 50,
                decoration: BoxDecoration(
                  color:
                      selected4 ? Colors.white : Color.fromRGBO(0, 165, 137, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
              ),
            ),
          ],
        ));
  }
}

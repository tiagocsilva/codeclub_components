import 'package:flutter/material.dart';
import 'task.dart';
import 'models/task_model.dart';

class TaskList extends StatefulWidget {
  final List<TaskModel> tasks;
  const TaskList({Key? key, required this.tasks}) : super(key: key);

  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  var tasks = [];

  @override
  void initState() {
    super.initState();
    tasks = this.widget.tasks;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(children: [
              Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  padding: EdgeInsets.only(bottom: 10),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Atividades em desenvolvimento',
                          style: TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.none,
                              fontSize: 20),
                        ),
                      ])),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: tasks.length,
                  itemBuilder: (context, index) {
                    return Task(tasks[index], () {
                      setState(() {
                        tasks.removeAt(index);
                      });
                    });
                  }),
            ])));
  }
}

import 'package:codeclub_components/models/task_model.dart';
import 'package:codeclub_components/video_task.dart';
import 'package:flutter/material.dart';
import 'social_icon_bar.dart';
import 'space_container.dart';
import 'app_button.dart';
import 'presence_control_list.dart';
import 'user.dart';
import 'login.dart';
import 'task_list.dart';
import 'occurrences.dart';
import 'material_item_list.dart';
import 'menu.dart';

void main() {
  runApp(AppMain());
}

class AppMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<TaskModel> tasks = [
      new TaskModel('Aula de Scratch', "Tarefa aberta a 04:00 horas"),
      new TaskModel('Desafio dos Perdidos', "Tarefa aberta a 04:00 horas"),
      new TaskModel('Aprenda a cria seu jogo', "Tarefa aberta a 04:00 horas"),
      new TaskModel('Python', "Tarefa aberta a 04:00 horas"),
      new TaskModel('Aula de Scratch Lousa Mágica', "Tarefa aberta a 04:00 horas")
    ];

    return MaterialApp(
        home: ListView(children: [
      Column(children: [
        SpaceContainer(),
        SocialIconBar(),
        SpaceContainer(),
        MaterialItemList(),
        SpaceContainer(),
        Occurrences(),
        User(),
        VideoTask("Título do video"),
        SpaceContainer(),
        Login(),
        SpaceContainer(),
        AppButton(
          text: 'Entrar',
          onPressed: () => {},
        ),
        SpaceContainer(),
        AppButton(
          text: 'Registrar frequencia',
          variant: 'secondary',
          onPressed: () => {},
        ),
        SpaceContainer(),
        TaskList(tasks: tasks),
        SpaceContainer(),
        PresenceControlList(),
        SpaceContainer(),
        Menu(),
        SpaceContainer(),

      ])
    ]));
  }
}

import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  MyTextInputState createState() => new MyTextInputState();
}

class MyTextInputState extends State<Login> {
  bool isSwitched = false;
  @override
  Widget build(BuildContext context) {
    return new Material(
        child: new Container(
      padding: EdgeInsets.all(12),
      child: Column(
        children: <Widget>[
          TextField(
            style: TextStyle(color: Color.fromRGBO(0, 165, 137, 1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(0, 165, 137, 1)),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(0, 165, 137, 1)),
              ),
              border: OutlineInputBorder(),
              labelText: 'Login',
            ),
          ),
          SizedBox(height: 16),
          TextField(
            style: TextStyle(color: Color.fromRGBO(0, 165, 137, 1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(0, 165, 137, 1)),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromRGBO(0, 165, 137, 1)),
              ),
              border: OutlineInputBorder(),
              labelText: 'Senha',
              isDense: true, // Added this
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Row(
                  children: [
                    Switch(
                      value: isSwitched,
                      onChanged: (value) {
                        setState(() {
                          isSwitched = value;
                          print(isSwitched);
                        });
                      },
//             activeTrackColor: Colors.yellow,
                      activeColor: Color.fromRGBO(0, 165, 137, 1),
                    ),
                    Text("Salvar Informações"),
                  ],
                ),
              ),
              Text("Esqueci minha senha"),
            ],
          ),
        ],
      ),
    ));
  }
}

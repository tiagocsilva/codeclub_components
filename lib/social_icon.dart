import 'package:flutter/material.dart';

class SocialIcon extends StatelessWidget {
  SocialIcon(this.iconAssetPath);

  final String iconAssetPath;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {},
      child: Image.asset(this.iconAssetPath, width: 20, height: 20),
      style: ElevatedButton.styleFrom(
        shape: CircleBorder(),
        padding: EdgeInsets.all(20),
        primary: Color.fromRGBO(0, 165, 137, 1),
      ),
    );
  }
}
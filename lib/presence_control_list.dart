import 'package:flutter/material.dart';
import 'presence_control_card.dart';

class PresenceControlList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var txtStyle = TextStyle(
        color: Color.fromRGBO(0, 165, 137, 1),
        fontSize: 18,
        decoration: TextDecoration.none);

    return Container(
        margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(children: [
          Container(
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              padding: EdgeInsets.all(30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: Text(
                    'Data da aula',
                    style: txtStyle,
                    textAlign: TextAlign.start,
                  )),
                  Expanded(
                      child: Container(
                          child: Text(
                    'Alunos frequentes',
                    style: txtStyle,
                    textAlign: TextAlign.center,
                  ))),
                  Expanded(
                      child: Text(
                    'Registro de frequência',
                    style: txtStyle,
                    textAlign: TextAlign.end,
                  ))
                ],
              )),
          PresenceControlCard(date: '06/04/2021'),
          PresenceControlCard(date: '05/04/2021', alunosFrequentes: 5),
          PresenceControlCard(date: '06/04/2021', alunosFrequentes: 5)
        ]));
  }
}

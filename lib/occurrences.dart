import 'package:codeclub_components/app_button.dart';
import 'package:flutter/material.dart';

class Occurrences extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var inputBorder = OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.all(Radius.circular(8)));

    return Container(
        margin: EdgeInsets.all(30),
        child: Column(
          children: [
            Card(
                elevation: 8,
                color: Color.fromRGBO(0, 165, 137, 1),
                child: TextField(
                    cursorColor: Colors.white,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Color.fromRGBO(0, 165, 137, 1),
                        hintText: 'Digite aqui o assunto',
                        hintStyle: TextStyle(color: Colors.white),
                        border: inputBorder))),
            Container(
              margin: EdgeInsets.only(top: 10),
            ),
            Card(
              elevation: 5,
              child: TextField(
                  cursorColor: Colors.white,
                  keyboardType: TextInputType.multiline,
                  minLines: 6,
                  maxLines: null,
                  decoration: InputDecoration(
                      hintText: 'Digite aqui a sua mensagem',
                      border: inputBorder)),
            ),
            Container(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.only(top: 15),
              child:
                  AppButton(text: 'Registrar ocorrência', onPressed: () => {}),
            ),
          ],
        ));
  }
}

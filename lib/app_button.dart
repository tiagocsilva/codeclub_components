import 'package:flutter/material.dart';

class AppButton extends StatefulWidget {
  final String text;
  final String? variant;
  final Function onPressed;

  const AppButton({Key? key, required this.text, required this.onPressed, this.variant})
      : super(key: key);

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  Color fontColor = Colors.white;
  Color bgColor = Color.fromRGBO(0, 165, 137, 1);

  @override
  Widget build(BuildContext context) {
    if (this.widget.variant == 'secondary') {
      this.bgColor = Colors.white;
      this.fontColor = Color.fromRGBO(74, 123, 158, 1);
    }

    return ElevatedButton(
      onPressed: () => this.widget.onPressed(),
      child: Text(this.widget.text,
          style: TextStyle(color: this.fontColor, fontWeight: FontWeight.bold)),
      style: ElevatedButton.styleFrom(
          elevation: 8,
          padding: EdgeInsets.fromLTRB(60, 23, 60, 23),
          primary: this.bgColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          )),
    );
  }
}

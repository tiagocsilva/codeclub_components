import 'package:flutter/material.dart';

class VideoTask extends StatelessWidget {
  VideoTask(this.caption);

  final String caption;

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(30),
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                    child: (Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                      TextButton.icon(
                          onPressed: () {},
                          style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Color.fromRGBO(0, 165, 137, 1))),
                          label: Text("Adicionar tarefa"),
                          icon: Icon(Icons.add_circle_rounded))
                    ]))),
                Container(
                    width: 30,
                    margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    child: Column(children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          height: 350,
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            caption,
                            textAlign: TextAlign.left,
                          ))
                    ])),
              ],
            )));
  }
}

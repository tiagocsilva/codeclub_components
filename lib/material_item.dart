import 'package:flutter/material.dart';

class MaterialItem extends StatelessWidget {
  final String title;

  MaterialItem(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 1, color: Color.fromRGBO(0, 0, 0, 0.05)))),
        child: Card(
            elevation: 0,
            child: Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                        child: (Row(children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          width: 90,
                          height: 90,
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                this.title,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Color.fromRGBO(0, 165, 137, 1)),
                              ),
                            ]),
                      )
                    ]))),
                    Container(
                      width: 80,
                      height: 80,
                      margin: EdgeInsets.all(10),
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.article,
                                size: 25,
                                color: Color.fromRGBO(0, 165, 137, 1)),
                            Container(margin: EdgeInsets.only(top: 5)),
                            Text(
                              'Download',
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color.fromRGBO(0, 165, 137, 1),
                                  decoration: TextDecoration.underline),
                            )
                          ],
                        ),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          padding: EdgeInsets.all(8),
                          primary: Color.fromRGBO(255, 255, 255, 1),
                        ),
                      ),
                    ),
                  ],
                ))));
  }
}

import 'package:codeclub_components/material_item.dart';
import 'package:flutter/material.dart';

class MaterialItemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const list = [
      'Aula de Scratch - 01',
      'Aula de Scratch - 02',
      'Aula de Scratch - 03',
      'Aula de Scratch - 04',
      'Aula de Scratch - 05'
    ];

    return Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Padding(
          padding: EdgeInsets.all(15),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: list.length,
              itemBuilder: (context, index) {
                return MaterialItem(list[index]);
              }),
        ));
  }
}

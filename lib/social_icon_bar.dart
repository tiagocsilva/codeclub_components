import 'package:flutter/material.dart';
import 'social_icon.dart';

class SocialIconBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SocialIcon('assets/images/facebook.png'),
        SocialIcon('assets/images/instagram.png'),
        SocialIcon('assets/images/linkedin.png'),
        SocialIcon('assets/images/youtube.png'),
      ],
    );
  }
}

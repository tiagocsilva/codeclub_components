import 'dart:math';

import 'package:codeclub_components/app_button.dart';
import 'package:flutter/material.dart';

class PresenceControlCard extends StatefulWidget {
  final String date;
  final int? alunosFrequentes;
  const PresenceControlCard(
      {Key? key, required this.date, this.alunosFrequentes})
      : super(key: key);

  @override
  _PresenceControlCardState createState() => _PresenceControlCardState();
}

class _PresenceControlCardState extends State<PresenceControlCard> {
  var txtStyle = TextStyle(color: Colors.white, fontSize: 18);
  var controlCompleted = false;
  int? alunosFrequentes;

  @override
  void initState() {
    super.initState();
    alunosFrequentes = this.widget.alunosFrequentes;
  }

  @override
  Widget build(BuildContext context) {
    if (alunosFrequentes != null) controlCompleted = true;

    return ConstrainedBox(
        constraints: BoxConstraints(minHeight: 100),
        child: Card(
            elevation: 10.0,
            color: Color.fromRGBO(42, 109, 154, 1),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(this.widget.date, style: txtStyle),
                    Text(
                        controlCompleted
                            ? alunosFrequentes.toString().padLeft(2, '0')
                            : '',
                        style: txtStyle),
                    controlCompleted
                        ? Text('Concluido', style: txtStyle)
                        : AppButton(
                            text: 'Registrar Frequencia',
                            variant: 'secondary',
                            onPressed: () => {
                                  setState(() {
                                    alunosFrequentes = Random().nextInt(20);
                                    controlCompleted = true;
                                  })
                                }),
                  ],
                ))));
  }
}

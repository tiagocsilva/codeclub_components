import 'package:flutter/material.dart';
import 'models/task_model.dart';

class Task extends StatelessWidget {
  final TaskModel task;
  final Function onDelete;

  Task(this.task, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 1, color: Color.fromRGBO(0, 0, 0, 0.05)))),
        child: Card(
            elevation: 0,
            child: Padding(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                        child: (Row(children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(15.0),
                        child: Container(
                          width: 90,
                          height: 90,
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                this.task.title,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              Text(
                                this.task.subtitle,
                                style: TextStyle(color: Colors.grey),
                              ),
                            ]),
                      )
                    ]))),
                    Container(
                      width: 30,
                      margin: EdgeInsets.all(10),
                      child: ElevatedButton(
                        onPressed: () {
                          this.onDelete();
                        },
                        child: Icon(Icons.delete, size: 18),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          padding: EdgeInsets.all(8),
                          primary: Color.fromRGBO(0, 165, 137, 1),
                        ),
                      ),
                    ),
                  ],
                ))));
  }
}

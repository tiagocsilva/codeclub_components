import 'package:flutter/material.dart';

class User extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        margin: EdgeInsets.all(50),
        child: Padding(
            padding: EdgeInsets.all(5),
            child: Row(
              children: [
                Image.asset('assets/images/user.png', width: 80, height: 80),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Leonardo da Silva',
                  ),
                )
              ],
            )));
  }
}
